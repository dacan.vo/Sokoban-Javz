# Projet


Voici un Sokoban en Java créé par Marin Gérard et Dac-An Vo dans le cadre du projet Génie logiciel orienté objet



### Installation

Ouvrir le dossier dans n'importe quel IDE Java


## Comment l'utiliser 

Ouvrir le fichier Fenetre du dossier Boundary et lancer le main

A la fin d'un niveau, une pop-up apparaitra pour vous faire accedez au niveau suivant.
L'ajout des niveaux est assez simple, il s'agit d'un fichier texte avec :
* V pour une case vide
* M pour un mur
* C pour une cible
* J pour le joueur.

Le fichier doit s'appeler niveauX.txt avec X le numéro de votre niveau.
Une modification de la valeur de la classe Maze (entity) est nécessaire (Une amélioration est nécessaire pour éviter cette étape)

## Authors

* Dac-An Vo
* Marin Gérard

