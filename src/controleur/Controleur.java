package controleur;
import java.io.IOException;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

import entity.Map;
import entity.Player;

//@objid ("45ac9a0e-4424-488f-bf26-72d399fdc62f")
public class Controleur {
    //@objid ("d2524cf5-e4db-4025-9f2b-7ff06113e078")
    public Map map;
    public Controleur() { //Initialisation du controleur avec le premier niveau
    	this.map=new Map();
    	try {
			this.map.InitFromText("Niveaux/niveau"+this.map.getNiveau()+".txt");
		} catch (IOException e) {
			System.out.println("Ca marche pas :(");
			e.printStackTrace();
		}
    	
    }
    
    public int getColonne() { // Renvoie le nb de colonne du niveau
    	return this.map.getColonne();
    }
    
    public int getLigne() { // Renvoie le nb de ligne du niveau
    	return this.map.getLigne();
    }
    
    public int getContenu(int i,int j) { // Renvoie le continu de la case (i,j)
    	return this.map.getContenu(i, j);
    }
    
    public int getColonneJoueur() { // Renvoie la position du Joueur (colonne)
    	return this.map.getColonneJoueur();
    }
    
    public int getLigneJoueur() { // Renvoie la position du Joueur (ligne)
    	return this.map.getLigneJoueur();
    }

    //@objid ("c26b87cf-7f95-4384-9d28-d8fb7a68c863")
    public void action(Direction dir) { //deplacement du joueur
    	switch(dir) {
    	case HAUT :
    		this.map.haut();
    		break;
    	case BAS :
    		this.map.bas();
    		break;
    	case DROITE :
    		this.map.droite();
    		break;
    	case GAUCHE :
    		this.map.gauche();
    		break;
    	}
    }
    public void Reset() throws IOException { // Reset du niveau
    	this.map.InitFromText("Niveaux/niveau"+this.map.getNiveau()+".txt");
    }
    
    public boolean jeuTermine() { //Termine le niveau
    	return this.map.jeutermine();
    }

	public void niveausup() { //Change de niveau
		
		this.map.setNiveau(1);
		
	}
	public int getNiveau() {
		return this.map.getNiveau();
	}
	public int getNiveauMax() {
		return this.map.getNiveauMax();
	}

}
