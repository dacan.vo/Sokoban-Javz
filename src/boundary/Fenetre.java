package boundary;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import controleur.Direction;
import controleur.Controleur;


public class Fenetre extends JFrame implements KeyListener,Runnable{

	 static final int TAILLE_IMAGE = 32;
	 private static final int LARGEUR_FENETRE = 20 * TAILLE_IMAGE;
	 private static final int HAUTERU_FENETRE = 12 * TAILLE_IMAGE;
	 private static final int HAUTEUR_TITRE_FENETRE = 20;
	 private Controleur controleur;
	 
	
	 
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println("oks");
		Direction direction = switch( e.getKeyCode() ) {
        case KeyEvent.VK_UP    -> Direction.HAUT;
        case KeyEvent.VK_DOWN  -> Direction.BAS;
        case KeyEvent.VK_LEFT  -> Direction.GAUCHE;
        case KeyEvent.VK_RIGHT -> Direction.DROITE;
        case KeyEvent.VK_R -> Direction.RESET;
        default                -> null;
    };
    if (direction == Direction.RESET) { //Recommencer le niveau si bouton R
    	try {
			controleur.Reset();
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
    	repaint();
    	return;
	}
    if( direction == null ) return;
    controleur.action( direction ); 
    repaint();
    if( controleur.jeuTermine() ) { //Regarde si le niveau est fini
    	controleur.niveausup();    //Changer de niveau
    	if (controleur.getNiveau() >controleur.getNiveauMax()) {
    		JOptionPane.showMessageDialog( this, "Félicitation, vous avez terminé le jeu !");
    		System.exit(0);
    		
    	}
    	else {
        JOptionPane.showMessageDialog( this, "Vous avez gagné ! Vous passez au niveau "+ controleur.getNiveau() );
        try {
			controleur.Reset();
		} catch (IOException e1) {
			
			e1.printStackTrace();
		
		}
    	repaint();
    	}
        
    }
    
    
		
	}
	

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		 this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	        this.setPreferredSize( new Dimension( LARGEUR_FENETRE, HAUTERU_FENETRE + HAUTEUR_TITRE_FENETRE ));
	        this.setTitle( "Sokoban" );
	        this.controleur = new Controleur();
	        this.add( new Panneau( controleur ));
	        this.addKeyListener( this );

	        this.pack();
	        this.setVisible( true );
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Fenetre());
	}
	

}
