package boundary;

import java.awt.Graphics;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


import controleur.Controleur;



public class Panneau extends JPanel{
	
	private static final int TAILLE_IMAGE = Fenetre.TAILLE_IMAGE;
	private Controleur controleur;
	
	
	public Panneau( Controleur controleur ) { //Récupère le controleur créé dans Fenetre
        this.controleur = controleur;
	}
	
	public void paint( Graphics g ) {
        super.paint( g );

        
        for( int l = 0; l < controleur.getLigne(); ++l ) { // Parcourir les lignes
            for( int c = 0; c < controleur.getColonne(); ++c ) { //Parcourir les colonnes
            	switch (controleur.getContenu(l,c)) { //Dessiner le contenu de la case (i,j)
            	
            	case 1 : 
            		try {
						g.drawImage(ImageIO.read( new File( "img/Caisse.jpg" )),c * TAILLE_IMAGE, l * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
					} catch (IOException e) {
					
						e.printStackTrace();
					}
            		break;
            		
            	case 2 : 
            		try {
						g.drawImage(ImageIO.read( new File( "img/Joueur.jpg" )),c * TAILLE_IMAGE, l * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
					} catch (IOException e) {
						
						e.printStackTrace();
					}
            		break;
            		
            	case 3 : 
            		try {
						g.drawImage(ImageIO.read( new File( "img/Mur.jpg" )),c * TAILLE_IMAGE, l * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
					} catch (IOException e) {
						
						e.printStackTrace();
					}
            		break;
            		
            	case 4 : 
            		try {
						g.drawImage(ImageIO.read( new File( "img/Rangement.jpg" )),c * TAILLE_IMAGE, l * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
					} catch (IOException e) {
			
						e.printStackTrace();
					}
            		break;
            		
            	case 5 : 
            		try {
						g.drawImage(ImageIO.read( new File( "img/Vide.jpg" )),c * TAILLE_IMAGE, l * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
					} catch (IOException e) {
						
						e.printStackTrace();
					}
            		
            		break;
            	case 6 : 
            		try {
						g.drawImage(ImageIO.read( new File( "img/CaisseRangee.jpg" )),c * TAILLE_IMAGE, l * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
					} catch (IOException e) {
						
						e.printStackTrace();
					}
            		break;
            	}
            	}
            }
        
        
        int ligneJoueur = this.controleur.getLigneJoueur(); //Récupérer la position du joueur
        int colonneJoueur = this.controleur.getColonneJoueur();
        try {  //Dessiner le joueur au bon endroit
			g.drawImage(ImageIO.read( new File( "img/Joueur.jpg" )),colonneJoueur * TAILLE_IMAGE, ligneJoueur * TAILLE_IMAGE, TAILLE_IMAGE, TAILLE_IMAGE, null );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
                
}
