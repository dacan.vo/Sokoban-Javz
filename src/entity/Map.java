package entity;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

//import com.modeliosoft.modelio.javadesigner.annotations.objid;

//@objid ("edb45e35-484b-4deb-b909-256e94a25dac")
public class Map {
    //@objid ("e666347d-d255-453b-ae43-7f4f0610cef8")
    private Case[][] map;
    private Player joueur;
    private int nbLigne ; 
    private int nbColonne;
    private int niveau=1;
    private int niveaumax=3;
    private int n_cibles;
    private int n_ok;
    
    public int getLigneJoueur() { // Retourne la positiçon du joueur (ligne)
    	return this.joueur.getLigne();
    }
    
    public int getColonneJoueur() { // Retourne la positiçon du joueur (colonne)
    
    	return this.joueur.getColonne();
    }
public void InitFromText(String fileName) throws IOException { //Initilalise la matrice map avec le fichier texte
    	
    	int nbrLig = 0;
		int nbrCol = 0;
		FileReader fr;
		fr = new FileReader(fileName);
		BufferedReader br = new BufferedReader(fr);  
		String str;
		while((str = br.readLine()) != null)
			  {
			
			  //Pour chaque ligne, on incrémente le nombre de lignes
			  nbrLig++;
			  nbrCol=str.length(); // on récupère le nombre de box de la dernière ligne
			         					            
			      }
		fr.close(); // On a récupéré le nombre de ligne et de colonne
		
		
    	this.nbLigne = nbrLig;  //On initialise notre matrice map avec les données du fichier texte
    	this.nbColonne = nbrCol ;
		Case[][] Lab = new Case[nbrLig][nbrCol];
	    map = Lab;
	    int i =0;
	    int j;
	    FileReader fr2;
		fr2 = new FileReader(fileName);
		BufferedReader br2 = new BufferedReader(fr2);  
		String str2;
	    Character m;
	    n_cibles=0;
	    n_ok=0;
	    while((str2 = br2.readLine()) != null)
		 	{//on lit le fichier ligne par ligne
	    	for ( j=0;j<nbrCol;j++) {//pour chaque box d'une ligne
	    	
	    		m = str2.charAt(j);
	    		if (Character.compare(m, 'M')==0) {//la case est un mur
	    			map[i][j]= new Case(i,j, true, false);
	    		}
	    		if (Character.compare(m, 'C')==0) {//la case est une cible
	    			map[i][j]= new Case(i,j, false, true);
	    			n_cibles+=1;
	    		}
	    		if (Character.compare(m, 'V')==0) {//la case est vide
	    			map[i][j]= new Case(i,j, false, false);
	    		}
	    		if (Character.compare(m, 'J')==0) {//la case est un joueur
	    			map[i][j]= new Case(i,j, false, false);
	    			this.joueur = new Player(i,j, false, false);
	    			
	    		}
	    		if (Character.compare(m, 'B')==0) {//la case est une boite
	    			map[i][j]= new Box(i,j, false, false);
	    		}
	    		
	    		} i++;//on passe à la ligne suivante
	    		
		 	}fr2.close();

    }

public int getLigne() {
	return nbLigne;
}

public int getColonne() {
	return nbColonne;
}
public int getContenu(int i,int j) { //Retourne les valeurs (1 à 6) en fonction du contenu de la case
	if (this.map[i][j] instanceof Box && this.map[i][j].EstCible == false) return 1;
	else if (this.map[i][j] instanceof Box && this.map[i][j].EstCible == true) return 6;
	else if (this.map[i][j] instanceof Player) return 2;
	else if (this.map[i][j].EstMur) return 3;
	else if (this.map[i][j].EstCible) return 4;
	else return 5;
}
public void haut() { //Appel la fonction déplacement pour aller en haut
	déplacement(-1,0);
}

public void bas() { //Appel la fonction déplacement pour aller en bas
	déplacement(1,0);
}

public void droite() { //Appel la fonction déplacement pour aller à droite
	déplacement(0,1);
}

public void gauche() {//Appel la fonction déplacement pour aller en gauche
	déplacement(0,-1);
}
public void déplacement(int l, int c) { //Fonction générale de déplacement 
	int i = this.joueur.getLigne();
	int j = this.joueur.getColonne();
	switch (getContenu(i+l,j+c)) {
	
	case 5 :
		this.joueur.setLigne(i+l);
		this.joueur.setColonne(j+c);
		break;
		
	case 4:
		this.joueur.setLigne(i+l);
		this.joueur.setColonne(j+c);
		break;
		
	case 3 :
		break;
		
	case 2 :
		System.err.println("Erreur, y a-t-il deux joueurs ? Je ne cromprends pas ce qu'il se passe...");
		break;
		
	case 1,6 :
		switch(getContenu(i+2*l,j+2*c)) {
		case 5: //Si la case d'après est vide
			this.joueur.setLigne(i+l); // On peut avancer
			this.joueur.setColonne(j+c);
			if (this.map[i+l][j+c].EstCible) { //si la boite est sur un emplacement avec une cible
				this.map[i+l][j+c]= new Case(i+l,j+c, false, true); // alors la case libérée devient une cible
				n_ok-=1;
			}
			else this.map[i+l][j+c]= new Case(i+l,j+c, false, false); // Sinon c'est une case vide
			this.map[i+2*l][j+2*c]=new Box(i+2*l,j+2*c,false,false); // Puis la case d'après devient une boîte (pas cible)
			break;
			
		case 4 : // Si la case d'apès est une cible, on fait la même chose mais en placant la boîte sur une cible.
			this.joueur.setLigne(i+l);
			this.joueur.setColonne(j+c);
			if (this.map[i+l][j+c].EstCible) {
				this.map[i+l][j+c]= new Case(i+l,j+c, false, true);
				n_ok-=1;
				
			}
			else this.map[i+l][j+c]= new Case(i+l,j+c, false, false);
			this.map[i+2*l][j+2*c]=new Box(i+2*l,j+2*c,false,true);
			n_ok+=1;
			break;
			
		case 3 :
			break;
		
		case 2 :
			System.err.println("Erreur, y a-t-il deux joueurs ? Je ne cromprends pas ce qu'il se passe...");
			break;
			
		case 1 :
			break;	
		}
	} //Fin du déplacement !
	}

public boolean jeutermine() { //Vérifie si le jeu est terminé
	return (n_ok==n_cibles);
}
	
public int getNiveau(){
	return this.niveau;
	}
public void setNiveau(int x) {
	this.niveau = this.niveau + x;
}
public int getNiveauMax() {
	return this.niveaumax;
}
}