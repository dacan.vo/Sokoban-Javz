package entity;
//import com.modeliosoft.modelio.javadesigner.annotations.objid;

//@objid ("f75781ba-a5a3-4901-a0ee-ec7030eddd1b")
public class Case{
    //@objid ("ca2da847-7a90-42fb-bf33-be82f79c0172")
    public int Ligne;

    //@objid ("52f56750-8f66-4f48-873d-16f5acf0ad27")
    public int Colonne;

    //@objid ("7a62d581-6bac-4b16-900d-627e50f0d477")
    public boolean EstMur;

    //@objid ("2d492f49-662f-4505-b4d2-ea4954934a33")
    public boolean EstCible;
    
    public Case(int ligne, int colonne, boolean estMur, boolean estCible) {
    	Ligne=ligne;
    	Colonne=colonne;
    	EstMur=estMur;
    	EstCible=estCible;
    }

    public int getLigne() {
    	return this.Ligne;
    }
    public int getColonne() {
    	return this.Colonne;
    }
    
    public boolean estMur() {
    	return this.EstMur;
    }
    public boolean estCible() {
    	return this.EstCible;
    }
    
    public void setLigne(int l) {
    	this.Ligne=l;
    }
    
    public void setColonne(int c) {
    	this.Colonne=c;
    }
    


}

